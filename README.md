# `script-url`

> Gets URL of currently running script (browser only) as a string.

## Usage

```js
import scriptUrl from "@helb/script-url";

console.log(scriptUrl()) // -> eg. "http://localhost:3000/script.js"