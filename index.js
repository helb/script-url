const isIE = () => navigator.appName === 'Microsoft Internet Explorer'
                   || !!(navigator.userAgent.match(/Trident/)
                   || navigator.userAgent.match(/rv:11/));

const scriptUrl = () => {
  if (isIE()) { // IE
    try {
      throw new Error();
    } catch (e) {
      return /[@(](.*\.js)/.exec(e.stack.split('\n')[1])[1];
    }
  } else {
    return new Error().fileName // Firefox
    || /[@(](.*\.js)/.exec(new Error().stack.split('\n')[1])[1]; // Chrome, Edge, …
  }
};

export default scriptUrl;
